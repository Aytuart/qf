export default function ({$axios, i18n}, inject) {
	inject('api', {
		
		getUrl(url) {
			return url.startsWith('http') ? url : `${$axios.defaults.baseURL}/${i18n.locale}/api/${url}`
		},
		
		post(url, data = {}, headers = {}, recaptcha = false, progress = true) {
			return $axios.$post(this.getUrl(url), data, {headers: headers, progress: progress})
		},
		
		get(url, data = {}, headers = {}, recaptcha = false, progress = true) {
			return $axios.$get(this.getUrl(url), { params: data }, {headers: headers, progress: progress})
		},

		put(url, data = {}, headers = {}, recaptcha = false, progress = true) {
			return $axios.$put(this.getUrl(url), data, headers, {headers: headers, progress: progress})
		},
		
		delete(url, data = {}, headers = {}, recaptcha = false, progress = true) {
			return $axios.$delete(this.getUrl(url), { data: data }, {headers: headers, progress: progress})
		}
	})
}