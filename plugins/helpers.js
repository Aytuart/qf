import moment from "moment";
import md5 from 'js-md5';

export default function({ store }, inject) {
    inject("isValidPhone", phone => {
        return phone && /^[\d\(\)\s\+\-]+$/.test(phone);
    });

    inject("decl", (number, txt, cases = [2, 0, 1, 1, 1, 2]) => {
        return txt[
            number % 100 > 4 && number % 100 < 20
                ? 2
                : cases[number % 10 < 5 ? number % 10 : 5]
        ];
    });

    inject("msToTime", duration => {
        let data = {};
        data.seconds = parseInt((duration / 1000) % 60);
        data.minutes = parseInt((duration / (1000 * 60)) % 60);
        data.hours = parseInt((duration / (1000 * 60 * 60)) % 24);
        data.days = parseInt(duration / (1000 * 60 * 60 * 24));
        data.hours = data.hours < 10 ? "0" + data.hours : data.hours;
        data.minutes = data.minutes < 10 ? "0" + data.minutes : data.minutes;
        data.seconds = data.seconds < 10 ? "0" + data.seconds : data.seconds;
        return data;
    });

    inject("mask", (string, pattern, ch = "#") => {
        let masked = "";
        for (let i = 0, j = 0; i < pattern.length; ++i) {
            let char = "";
            if (pattern[i] == ch) {
                char = j < string.length ? string[j++] : "";
            } else if (j < string.length) {
                char = pattern[i];
            }
            masked += char;
        }
        return masked;
    });

    inject("get_hours", seconds => {
        return Math.floor(seconds / 3600);
    });

    inject("get_minutes", seconds => {
        return Math.floor((seconds % 3600) / 60);
    });

    inject("get_seconds", seconds => {
        return (seconds % 3600) % 60;
    });

    moment.locale("ru");
    inject("format_date", (value, format = "DD.MM.YYYY") => {
        return moment(value).format(format);
    });

    inject("format_phone", string => {
        let cleaned = ("" + string).replace(/\D/g, "");
        let match = cleaned.match(/^(7|)?(\d{3})(\d{3})(\d{2})(\d{2})$/);
        if (match) {
            let intlCode = match[1] ? "+7 " : "";
            return [
                intlCode,
                "(",
                match[2],
                ") ",
                match[3],
                " ",
                match[4],
                " ",
                match[5]
            ].join("");
        }
        return null;
    });

    inject("format_number", number => {
        return number
            ? number
                  .toLocaleString()
                  .split(",")
                  .join(" ")
            : number;
    });

    inject("str_limit", (str, cnt = 50) => {
        return (str ? str.substring(0, cnt) : str) + "...";
    });

    inject("pad", (str, size = 2, ch = "0") => {
        let s = str + "";
        while (s.length < size) s = ch + s;
        return s;
    });

    inject("settings", key => {
        return store.getters["settings/GET_BY_KEY"](key);
    });

    inject("ucFirst", str => {
        if (!str) return str;
        return str[0].toUpperCase() + str.slice(1);
    });

	inject("mobile", () => store.getters['getIsMobile']),

	inject('translate', (string) => {
		const translations = store.getters['translations/getTranslations']
		const t = translations.filter((item) => item.hash == md5(string))
		return t ? t[0].content : string 
	})
}
