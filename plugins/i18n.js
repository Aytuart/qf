export default function ({ app }) {
	app.i18n.onLanguageSwitched = (oldLocale, newLocale) => {
		app.store.dispatch('translations/fetchTranslations')
	}
  }