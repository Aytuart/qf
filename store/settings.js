export const state = () => ({
	settings: null
})

export const getters = {
	getSettings: state => state.settings
}

export const mutations = {
	setSettings: (state, settings) => state.settings = settings
}

export const actions = {
	async fetchSettings({ commit }) {
		commit('setSettings', await this.$api.get('settings'))
	}
}