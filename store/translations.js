export const state = () => ({
	translations: null
})

export const getters = {
	getTranslations: state => state.translations
}

export const mutations = {
	setTranslations: (state, translations) => state.translations = translations
}

export const actions = {
	async fetchTranslations({ commit }) {
		commit('setTranslations', await this.$api.get('translations'))
	}
}