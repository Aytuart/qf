import webpack from 'webpack'
export default {
	// Global page headers: https://go.nuxtjs.dev/config-head
	head: {
		title: 'qazaqfruit',
		htmlAttrs: {
			lang: 'en'
		},
		meta: [
			{ charset: 'utf-8' },
			{ name: 'viewport', content: 'width=device-width, initial-scale=1' },
			{ hid: 'description', name: 'description', content: '' },
			{ name: 'format-detection', content: 'telephone=no' }
		],
		link: [
			{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
			{
				src: "https://code.jquery.com/jquery-3.3.1.min.js",
				type: "text/javascript"
			},
		],
	},
	
	// Global CSS: https://go.nuxtjs.dev/config-css
	css: [
		'@/assets/css/app.sass'
	],
	
	pageTransition: 'fade',
	
	// Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
	plugins: [
		{ src: '@/plugins/swiper', mode: 'client' },
		{ src: '@/plugins/api' },
		{ src: '@/plugins/flags' },
		{ src: '@/plugins/helpers' },
		{ src: '@/plugins/i18n' },
	],
	
	// Auto import components: https://go.nuxtjs.dev/config-components
	components: true,
	
	// Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
	buildModules: [
		"@nuxtjs/svg"
	],
	
	// Modules: https://go.nuxtjs.dev/config-modules
	modules: [
		// https://go.nuxtjs.dev/axios
		'@nuxtjs/axios',
		'@nuxtjs/i18n'
	],
	
	i18n: {
		defaultLocale: "ru",
		locales: [
			'ru',
			'en',
			'kk'
		]
	},
	
	// Axios module configuration: https://go.nuxtjs.dev/config-axios
	axios: {
		// Workaround to avoid enforcing hard-coded localhost:3000: https://github.com/nuxt-community/axios-module/issues/308
		baseURL: 'https://admin.qazaqfruit.brandstudio.kz',
	},
	
	// Build Configuration: https://go.nuxtjs.dev/config-build
	build: {
		transpile: ['gsap']
	},
}
